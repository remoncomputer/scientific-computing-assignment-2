# Description
This is assignment 2 in the Scientific computing Course Fall 2018

# Requirements
- Install Visual Studio 2017 Community Edition
	- Install C++ For Desktop Applications
		- Install MFC Library and C++\CLI 
	- Install Github for windows

# opening the project
- Open Mass-Spring Simulation.sln using Visual Studio

# Notes
You should be able to run the executable from Visual studio in Debug and release modes
