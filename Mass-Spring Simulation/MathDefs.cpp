
#include "stdafx.h"
#include <math.h>
#include "mathdefs.h"

///////////////////////////////////////////////////////////////////////////////
// Function:	MultVectorByMatrix
// Purpose:		Multiplies a vector by a 4x4 Matrix in OpenGL Format
// Arguments:	Matrix, Vector in, and result Vector
// Notes:		This routing is tweaked to handle OpenGLs column-major format
//				This is one obvious place for optimization perhaps asm code
///////////////////////////////////////////////////////////////////////////////
void MultVectorByMatrix(tMatrix *mat, tVector *v,tVector *result)
{
	result->x = (mat->m[0] * v->x) +
			   (mat->m[4] * v->y) +	
			   (mat->m[8] * v->z) +
			   mat->m[12];
	result->y = (mat->m[1] * v->x) +
			   (mat->m[5] * v->y) +	
			   (mat->m[9] * v->z) +
			   mat->m[13];
	result->z = (mat->m[2] * v->x) +
			   (mat->m[6] * v->y) +	
			   (mat->m[10] * v->z) +
			   mat->m[14];
}
//// MultVectorByMatrix //////////////////////////////////////////////////////


/* returns squared length of input vector */    
double VectorSquaredLength(tVector *v) 
{
	return((v->x * v->x) + (v->y * v->y) + (v->z * v->z));
}

/* returns length of input vector */
double VectorLength(tVector *v) 
{
	return(sqrt(VectorSquaredLength(v)));
}

/* destructively normalizes the input vector */
void NormalizeVector(tVector *v) 
{
	float len = (float)VectorLength(v);
    if (len != 0.0) 
	{ 
		v->x /= len;  
		v->y /= len; 
		v->z /= len; 
	}
}

double DotProduct(tVector *v1, tVector *v2)
{
	return ((v1->x * v2->x) + (v1->y * v2->y) + (v1->z * v2->z));
}

/* return the cross product result = v1 cross v2 */
void CrossProduct(tVector *v1, tVector *v2, tVector *result)
{
	result->x = (v1->y * v2->z) - (v1->z * v2->y);
	result->y = (v1->z * v2->x) - (v1->x * v2->z);
	result->z = (v1->x * v2->y) - (v1->y * v2->x);
}

double VectorSquaredDistance(tVector *v1, tVector *v2) 
{
	return(	((v1->x - v2->x) * (v1->x - v2->x)) + 
			((v1->y - v2->y) * (v1->y - v2->y)) + 	
			((v1->z - v2->z) * (v1->z - v2->z)) ); 	
}

void ScaleVector(tVector *v, float scale, tVector *result) 
{
	result->x = v->x * scale;
	result->y = v->y * scale;
	result->z = v->z * scale;
}

void VectorSum(tVector *v1, tVector *v2, tVector *result) 
{
	result->x = v1->x + v2->x;
	result->y = v1->y + v2->y;
	result->z = v1->z + v2->z;
}

void VectorDifference(tVector *v1, tVector *v2, tVector *result, int n) 
{
	if (n > 0)
	{
		v1->x = pow(2, n) * v1->x;
		v1->y = pow(2, n) * v1->y;
		v1->z = pow(2, n) * v1->z;
	}

	result->x = v1->x - v2->x;
	result->y = v1->y - v2->y;
	result->z = v1->z - v2->z;
}

//added by Yara
void DivVectorbyVector(tVector *v, tVector *scale, tVector *result, int n)
{
	float eps;
	eps = 0.001;
	if (n == 0)
	{
		result->x = abs(v->x / (scale->x + eps) );
		result->y = abs(v->y / (scale->y + eps) );
		result->z = abs(v->z / (scale->z + eps) );
	}
	// Ghadir
	else
	{
		result->x = abs(v->x / (pow(2, n) - 1));
		result->y = abs(v->y / (pow(2, n) - 1));
		result->z = abs(v->z / (pow(2, n) - 1));
	}
}

//added by Yara
float SumtVectorCoord(tVector *tVec) {

	float error, error_y, error_z;
	error = 0;
	error += tVec->x;
	error += tVec->y;
	error += tVec->z;
	return error;
}